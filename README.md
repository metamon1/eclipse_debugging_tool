* * *
"""본 저장소는 과학기술통신부의 재원으로 정보통신기획평가원(IITP)의 지원을 받아 (주) 드림에이스, 경북대학교, 대구경북과학기술원이 함께 수행한 "차량 ECU 응용소프트웨어 개발 및 검증자동화를 위한 가상 ECU기반 차량레벨 통합 시뮬레이션 기술개발(과제고유번호: 2710008421)" 과제의 일환으로 공개된 오픈소스 코드입니다. (2022.04~2024.12)"""

"""본 저장소는 Eclipse Open OCD Debugging Tool를 위한 것으로 이클립스 설치부터 환경에 대한 라이브러리 설치, 권한 설정 과정을 포함하고 있어 이클립스를 통한 디버깅을 위해 활용합니다."""
* * *

# 1. JDK 설치

Oracle JDK 설치의 경우  

### 1.1 Repository 추가

`$ sudo add-apt-repository ppa:webupd8team/java`  

### 1.2 Repository index 업데이트

`$ sudo apt-get update`  

### 1.3 JDK 설치 (Java 8 버전)

`$ sudo apt-get install oracle-java8-installer`  

# 2. C/C++ 개발 관련 패키지 설치

## 2.1 관련 패키지 설치

`$ sudo -s`  

# sudo apt-get install build-essential

※ Ubuntu 16.04.2 설치 상태에서 위 명령 실행해보면 이미 최신 버전이 설치 되어 있다고 나옴  

![1](/uploads/7f68be44a2b3c7c7cd05b6943fb0c5ce/1.png)  

# 3. Eclipse install

[Eclipse IDE for Embedded C/C++ Developers | Eclipse Packages](https://www.eclipse.org/downloads/packages/release/2022-06/r/eclipse-ide-embedded-cc-developers)  

위 링크를 통해 Eclipse IDE for Embedded C/C++ Developers 다운로드  

### 3.1 [Eclipse 공식 사이트](http://www.eclipse.org/) "Eclipse IDE for Embedded C/C++ Developers" 다운로드

운영체제 아키텍쳐 확인  

32비트 - i386, i486, i586, i686, x86  
64비트 - amd64, x86_64  
`$ uname -m`  



![2](/uploads/b43c7da04d688c7d523c52138cb8e966/2.png)  

### 3.2 다운받은 경로 이동 후 압축 해제

`$ tar xvzf (다운받은파일명)`  

### 3.3 Eclipse 폴더를 /opt 폴더로 이동

`$ sudo mv eclipse /opt`  

### 3.4 터미널 실행 설정  (둘 중 택 1)

`$ sudo vi /usr/bin/eclipse` (CUI를 사용하는 vi 사용)  
`$ sudo gedit /usr/bin/eclipse` (GUI를 사용하는 gedit 사용)  

```
#! /bin/sh

export ECLIPSE_HOME=/opt/eclipse
$ECLIPSE_HOME/eclipse $*
```
    
### 3.5 바로가기 설정에 대한 권한 설정

`$ sudo chmod 755 /usr/bin/eclipse`  

### 3.6 X윈도우 바로가기 설정 (둘 중 택 1)

`$ sudo vi /usr/share/applications/eclipse.desktop` (vi 사용)  

`$ sudo gedit /usr/share/applications/eclipse.desktop` (gedit 사용)    

```
[Desktop Entry]
Encoding=UTF-8
Name=Eclipse
Comment=Eclipse IDE
Exec=eclipse
Icon=/opt/eclipse/icon.xpm
Terminal=false
Type=Application
Categories=Development
StartupNotif=true
```

# 4. Eclipse 실행

## 4.1 터미널에서 정상실행 확인

`$ eclipse`  



![3](/uploads/6c3928635d182694dee3ad922ea12c9c/3.png)  

위 화면이 나오면 성공  

Launch를 눌러 메인화면으로  

# 5. versatilepb Eclipse Debugging

[AS stduy platform -- qemu - AS](https://autoas.github.io/as/autosar/2018/02/20/as-study-platform.html)  

해당 링크 3.7부터의 과정 서술  

## 5.1 BOARD, RELEASE 설정

`$ export BOARD=versatilepb`  

`$ export RELEASE=ascore`  

`$ scons run gdb`  


![4](/uploads/1b4e0d9dcd9b5961f0ab971594c55a01/4.png)  

`scons run gdb` 실행 시  

## 5.2 Debugging 과정

Eclipse 실행 시 메인 화면에서 상단바에 Run 클릭  


![5](/uploads/4a16109104226de00a51606e29f7959e/5.png)  

Debugging Configurations 클릭  


![6](/uploads/676606f0d93e036261a7ffb7d9a6315d/6.png)  

GDB QEMU Debugging 더블 클릭  


![7](/uploads/f4a2cea20e67f483388558619633afc7/7.png)  

1. Project: as  
2. C/C++ Application  
    
    Browse… 클릭 후 타켓 보드 선택 (예시의 경우 versatilepb)  
    
     ../as/build/posix/versatilepb/ascore/versatilepb.exe 선택  
    
3. Disable auto build 선택  

↓ 아래 처럼 설정  


![8](/uploads/76119a3b85e881d2a42c52e49a862c62/8.png)  

두번 째 탭 Debugger 클릭  

1. Start QEMU locally 체크 해제  
2. GDB Client Setup 부분 Browser 클릭 후 아래 링크에 있는 exe 파일 선택  

../autosa/release/download/gcc-arm-none-eabi-5_4-2016q3-20160926-win32/bin/arm-none-eabi-gdb.exe  


![9](/uploads/b53b7366d49645266d8d40749e96dc6c/9.png)  

## 5.3 Debug


![10](/uploads/7af4a914d21aff09978920665ca5513d/10.png)  

디버그 실행 시 코드 import  


![11](/uploads/1c818c0a3486b9b1915b4c8ddf7118c1/11.png)      

위 화면처럼 나오면 setting 성공  

## 5.4 단축어

Ctrl + F2 →  Terminate  

F8 → Resume  


# Plugin 관련 정리
https://www.notion.so/drimaes/Eclipse-Plugin-f617221766ff42b2965a8bb6ae469280?pvs=4
