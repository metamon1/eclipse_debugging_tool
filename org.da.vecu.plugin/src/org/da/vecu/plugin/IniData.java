package org.da.vecu.plugin;

import java.util.ArrayList;
import java.util.List;

public class IniData {
    public static final String KEY_EXPORT_BOARD = "export_board";
    public static final String KEY_BUILD_CMD_RUN = "cmd_run";
    public static final String KEY_BUILD_CMD_MONITORING_RUN = "cmd_monitoring_run";
    public static final String KEY_BUILD_CMD_BUILD = "cmd_build";
    public static final String KEY_BUILD_CMD_CLEAN = "cmd_clean";
    public static final String KEY_BUILD_CMD_LINSOCK = "cmd_linsock";
    public static final String KEY_BUILD_CMD_STUDIO = "cmd_studio";
    public static final String KEY_CONNMGRCLIENT_PATH = "conn_mgr_client_path";

    public enum BoardType{
        STM32F407ZG("TFM"),
        RASPBERRY_PI("TFM4e"),
        POSIX("TCM");
    	
        private final String boardName;

        BoardType(String boardName) {
            this.boardName = boardName;
        }

        public String getBoardName() {
            return boardName;
        }

        public static BoardType fromString(String text) {
            for (BoardType b : BoardType.values()) {
                if (b.boardName.equalsIgnoreCase(text)) {
                    return b;
                }
            }
            throw new IllegalArgumentException("No constant with text " + text + " found");
        }
        
        public String getDefaultValue(String key){
            switch (this){
                case STM32F407ZG:
                    return getStm32Default(key);
                case RASPBERRY_PI:
                    return getRaspberryPiDefault(key);
                case POSIX:
                    return getPosixDefault(key);
                default:
                    return "";
            }
        }

        private String getStm32Default(String key){
            switch (key){
                case KEY_BUILD_CMD_RUN: return "scons run";
                case KEY_BUILD_CMD_MONITORING_RUN: return "scons run QT qmp";
                case KEY_BUILD_CMD_BUILD: return "scons -j16";
                case KEY_BUILD_CMD_CLEAN: return "scons -c";
                case KEY_BUILD_CMD_LINSOCK: return "bash linbus.sh";
                case KEY_BUILD_CMD_STUDIO: return "scons studio";
                case KEY_CONNMGRCLIENT_PATH: return "";
                default: return "";
            }
        }

        private String getRaspberryPiDefault(String key){
        	switch (key){
        	case KEY_BUILD_CMD_RUN: return "bash run.sh";
        	case KEY_CONNMGRCLIENT_PATH: return "";
        	default: return "";
        	}
        }
        
        private String getPosixDefault(String key){
            switch (key){
                case KEY_BUILD_CMD_RUN: return "scons run";
                case KEY_BUILD_CMD_BUILD: return "scons --cluster";
                case KEY_BUILD_CMD_CLEAN: return "scons -c";
                case KEY_BUILD_CMD_LINSOCK: return "bash run_.sh";
                case KEY_BUILD_CMD_STUDIO: return "scons studio";
                case KEY_CONNMGRCLIENT_PATH: return "";
                default: return "";
            }
        }

    }
    
	public String CurrentValue = null;
	public String Key;
	public String DefaultValue;
	public IniData()
	{
		
	}
	public IniData(String key, String defValue)
	{
		Key = key;
		DefaultValue = defValue;
	}
	
    public static String getDefaultValue(String board, String key){
        try{
            BoardType boardType = BoardType.valueOf(board.toUpperCase());
            return boardType.getDefaultValue(key);
        }catch (IllegalArgumentException e){
            return "";
        }
    }
    
    public String GetValueString(){
        if(CurrentValue == null)
            CurrentValue = DefaultValue;
        return CurrentValue;
    }

	public List<String> GetValueList(){
		if(CurrentValue == null)
			CurrentValue = DefaultValue;
		List<String> arr = new ArrayList<String>();
		String[] tok = CurrentValue.split(" ");
		for (int i = 0; i < tok.length; i++) {
			String item = tok[i].trim();
			if (item.equals(""))
				continue;
			arr.add(item);
		}
		return arr;
	}
}
