
package org.da.vecu.plugin;

import javax.inject.Inject;
import javax.annotation.PostConstruct;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Composite;

public class SconsLogView {
	public enum PrintType {
		Log("[Log] "),
		Info("[Info] "),
		Error("[Error] "),
		Warning("[Warning] "),
		Debug("[Debug] "),
		None("[None] ");

		private final String value;

		private PrintType(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value;
		}
	}
	private StyledText sconsLogTextView;
	public static final String ID = "org.da.vecu.plugin.partdescriptor.sconslog";

	@Inject
	public SconsLogView() {

	}

	@PostConstruct
	public void postConstruct(Composite parent) {
		sconsLogTextView = new StyledText(parent, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL | SWT.WRAP);
		sconsLogTextView.setEditable(false);
	}

	public void Append(String s) {
		sconsLogTextView.append(s);
		sconsLogTextView.append("\n");
		SetLastCursor();
	}

	public void DebugPrint(String s) {
		if (ViewUtil.DEBUG == false)
			return;
		sconsLogTextView.append("[Debug]");
		sconsLogTextView.append(s);
		sconsLogTextView.append("\n");
		SetLastCursor();
	}
	
	public void Print(PrintType type, String s) {
		if (type == PrintType.Debug && !ViewUtil.DEBUG)
			return;

		String p = String.format("%s%s\n", type.toString(), s);
		sconsLogTextView.append(p);
		SetLastCursor();
	}


	public void SetLastCursor() {
		/*
		 * Display display = new Display(); display.timerExec(100, () -> {
		 * sconsLogTextView.setSelection(sconsLogTextView.getCharCount() - 1); });
		 * sconsLogTextView.redraw();
		 */
		sconsLogTextView.setSelection(sconsLogTextView.getCharCount() - 1);
		//DebugPrint("[" + sconsLogTextView.getCharCount() + "]\n");
	}

	public void setFocus() {
		sconsLogTextView.setFocus();
	}

}