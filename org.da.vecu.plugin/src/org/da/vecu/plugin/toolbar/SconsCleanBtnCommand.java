
package org.da.vecu.plugin.toolbar;

import org.da.vecu.plugin.CommandExecutor;
import org.da.vecu.plugin.FileLogMgr;
import org.da.vecu.plugin.ViewUtil;
import org.da.vecu.plugin.SconsLogView.PrintType;
import org.eclipse.e4.core.di.annotations.Execute;

public class SconsCleanBtnCommand {
	@Execute
	public void execute() {

		String consoleLog = CommandExecutor.Clean();

		ViewUtil.Print2(PrintType.Info, consoleLog);
//		ViewUtil.Print2(PrintType.Info, "Clean successful.");
        if (isBuildSuccessful(consoleLog)) {
            ViewUtil.Print2(PrintType.Info, "Clean successful.");
        } else {
            ViewUtil.Print2(PrintType.Info, "Clean fail.");
        }
		FileLogMgr.Logging("Clean", consoleLog);
	}
	   private boolean isBuildSuccessful(String consoleLog) {
	        String[] lines = consoleLog.split("\n");
	        String lastLine = lines[lines.length - 1].trim();

	        return lastLine.contains("scons: done cleaning targets");
	    }
}