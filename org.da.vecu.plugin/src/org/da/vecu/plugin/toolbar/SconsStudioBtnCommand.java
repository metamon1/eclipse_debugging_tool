
package org.da.vecu.plugin.toolbar;

import org.da.vecu.plugin.CommandExecutor;
import org.da.vecu.plugin.FileLogMgr;
import org.da.vecu.plugin.ViewUtil;
import org.da.vecu.plugin.SconsLogView.PrintType;
import org.eclipse.e4.core.di.annotations.Execute;

public class SconsStudioBtnCommand {
	@Execute
	public void execute() {
		String consoleLog = CommandExecutor.Studio();
		ViewUtil.Print2(PrintType.Info, consoleLog);
		ViewUtil.Print2(PrintType.Info, "Run Studio.");
		FileLogMgr.Logging("Studio", consoleLog);
	}
}