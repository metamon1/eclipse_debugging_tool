
package org.da.vecu.plugin.toolbar;

import org.da.vecu.plugin.CommandExecutor;
import org.da.vecu.plugin.FileLogMgr;
import org.da.vecu.plugin.ViewUtil;
import org.da.vecu.plugin.SconsLogView.PrintType;
import org.eclipse.e4.core.di.annotations.Execute;

public class SconsCleanBuildBtnCommand {
	@Execute
	public void execute() {

		String consoleLog1 = CommandExecutor.Clean();
		ViewUtil.Print2(PrintType.Info, consoleLog1);
		
		String consoleLog2 = CommandExecutor.Build();
		ViewUtil.Print2(PrintType.Info, consoleLog2);
//		ViewUtil.Print2(PrintType.Info, "Clean build successful.");
        if (isBuildSuccessful(consoleLog2)) {
            ViewUtil.Print2(PrintType.Info, "Clean Build successful.");
        } else {
            ViewUtil.Print2(PrintType.Info, "Clean Build fail.");
        }
		FileLogMgr.Logging("CleanBuild", String.format("%s\n%s", consoleLog1,consoleLog2));
	}
	
	   private boolean isBuildSuccessful(String consoleLog) {
	        String[] lines = consoleLog.split("\n");
	        String lastLine = lines[lines.length - 1].trim();

	        return lastLine.contains("scons: done building targets");
	    }

}