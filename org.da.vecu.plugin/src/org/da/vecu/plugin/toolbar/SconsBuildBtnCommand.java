
package org.da.vecu.plugin.toolbar;

import org.da.vecu.plugin.CommandExecutor;
import org.da.vecu.plugin.FileLogMgr;
import org.da.vecu.plugin.SconsLogView.PrintType;
import org.da.vecu.plugin.ViewUtil;
import org.eclipse.e4.core.di.annotations.Execute;

public class SconsBuildBtnCommand {
	@Execute
	public void execute() {
		
		String consoleLog = CommandExecutor.Build();

		ViewUtil.Print2(PrintType.Info, consoleLog);
//		ViewUtil.Print2(PrintType.Info, "Build successful.");
        if (isBuildSuccessful(consoleLog)) {
            ViewUtil.Print2(PrintType.Info, "Build successful.");
        } else {
            ViewUtil.Print2(PrintType.Info, "Build fail.");
        }
		FileLogMgr.Logging("Build", consoleLog);
	}
	   private boolean isBuildSuccessful(String consoleLog) {
	        String[] lines = consoleLog.split("\n");
	        String lastLine = lines[lines.length - 1].trim();

	        return lastLine.contains("scons: done building targets");
	    }
}