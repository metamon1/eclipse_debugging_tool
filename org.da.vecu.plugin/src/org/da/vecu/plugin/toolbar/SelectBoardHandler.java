package org.da.vecu.plugin.toolbar;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Shell;
import org.da.vecu.plugin.AvtEnvIniParser;
import org.da.vecu.plugin.IniData.BoardType;

public class SelectBoardHandler {

    @Execute
    public void execute(Shell shell) {
        BoardSelectionDialog dialog = new BoardSelectionDialog(shell);
        if (dialog.open() == Dialog.OK) {
            BoardType selectedBoard = dialog.getSelectedBoard();
            AvtEnvIniParser.setCurrentBoard(selectedBoard.getBoardName());
        }
    }

    private class BoardSelectionDialog extends Dialog {
        private BoardType selectedBoard;
        private Button tfmButton;
        private Button tfm4eButton;
        private Button tcmButton;
        private Button monitoringSwitch;

        public BoardSelectionDialog(Shell parentShell) {
            super(parentShell);
        }
        
        @Override
        protected void configureShell(Shell newShell) {
            super.configureShell(newShell);
            newShell.setText("Board Select");
        }
        
        @Override
        protected void setShellStyle(int newShellStyle) {
            super.setShellStyle(SWT.CLOSE | SWT.TITLE | SWT.BORDER | SWT.APPLICATION_MODAL | SWT.RESIZE);
        }

        @Override
        protected Point getInitialSize() {
            return new Point(400, 200); // 너비, 높이 (픽셀 단위)
        }

        @Override
        protected Control createDialogArea(Composite parent) {
            Composite container = (Composite) super.createDialogArea(parent);
            container.setLayout(new GridLayout(1, false));

            // 상단 구분선
            Label topSeparator = new Label(container, SWT.HORIZONTAL | SWT.SEPARATOR);
            topSeparator.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

            tfmButton = new Button(container, SWT.RADIO);
            tfmButton.setText("TFM");
            tfmButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

            // 모니터링 스위치 추가
            monitoringSwitch = new Button(container, SWT.CHECK);
            monitoringSwitch.setText("Enable Monitoring");
            monitoringSwitch.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
            monitoringSwitch.setSelection(AvtEnvIniParser.isUseMonitoring());
            monitoringSwitch.setEnabled(false);  // 초기에는 비활성화
            
            // TFM 선택 시에만 모니터링 스위치 활성화
            tfmButton.addListener(SWT.Selection, event -> {
                monitoringSwitch.setEnabled(tfmButton.getSelection());
            });

            // TFM과 TFMe 사이 구분선
            Label middleSeparator = new Label(container, SWT.HORIZONTAL | SWT.SEPARATOR);
            middleSeparator.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

            tfm4eButton = new Button(container, SWT.RADIO);
            tfm4eButton.setText("TFM4E");
            tfm4eButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

            // TFMe와 TCM 사이 구분선
            Label bottomSeparator = new Label(container, SWT.HORIZONTAL | SWT.SEPARATOR);
            bottomSeparator.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

            tcmButton = new Button(container, SWT.RADIO);
            tcmButton.setText("TCM");
            tcmButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

            // 하단 구분선
            Label finalSeparator = new Label(container, SWT.HORIZONTAL | SWT.SEPARATOR);
            finalSeparator.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

            // TFM 선택 시에만 모니터링 스위치 활성화
            tfmButton.addListener(SWT.Selection, event -> {
                monitoringSwitch.setEnabled(tfmButton.getSelection());
            });

            // 현재 선택된 보드에 따라 초기 선택 설정
            String currentBoard = AvtEnvIniParser.getCurrentBoard();
            if (currentBoard.equalsIgnoreCase("tfm")) {
            	tfmButton.setSelection(true);
                monitoringSwitch.setEnabled(true);
                monitoringSwitch.setSelection(AvtEnvIniParser.isUseMonitoring());
            } else if (currentBoard.equalsIgnoreCase("tfm4e")) {
            	tfm4eButton.setSelection(true);
            } else if (currentBoard.equalsIgnoreCase("tcm")) {
            	tcmButton.setSelection(true);
            }

            return container;
        }

        @Override
        protected void createButtonsForButtonBar(Composite parent) {
            createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
            createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
        }

        @Override
        protected void okPressed() {
            if (tfmButton.getSelection()) {
                selectedBoard = BoardType.STM32F407ZG;
                AvtEnvIniParser.setUseMonitoring(monitoringSwitch.getSelection());
            } else if (tfm4eButton.getSelection()) {
                selectedBoard = BoardType.RASPBERRY_PI;
            } else if (tcmButton.getSelection()) {
                selectedBoard = BoardType.POSIX;
            }
            super.okPressed();
        }

        public BoardType getSelectedBoard() {
            return selectedBoard;
        }

//        public boolean isMonitoringEnabled() {
//            return monitoringSwitch.getSelection() && tfmButton.getSelection();
//        }
    }
}