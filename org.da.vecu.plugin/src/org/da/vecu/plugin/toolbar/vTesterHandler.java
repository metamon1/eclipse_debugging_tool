package org.da.vecu.plugin.toolbar;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class vTesterHandler {
    @Execute
    public void execute(Shell shell) {
        // 새로운 스레드에서 Python 스크립트 실행
        new Thread(() -> {
            try {
            	// .sh 파일 실행
                ProcessBuilder pb = new ProcessBuilder("/bin/bash", "./vTester.sh");
                pb.redirectErrorStream(true);
                Process p = pb.start();
                
                BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

                String line;
                while ((line = reader.readLine()) != null) {
                    final String output = line;
                    Display.getDefault().asyncExec(() -> {
                        // UI 업데이트 (예: 로그 뷰에 출력)
                        System.out.println(output);
                    });
                }

                int exitVal = p.waitFor();
                Display.getDefault().asyncExec(() -> {
                    // UI 업데이트 (예: 실행 완료 메시지 표시)
                    System.out.println("Exit Value: " + exitVal);
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }
}