
package org.da.vecu.plugin.toolbar;

import org.da.vecu.plugin.CommandExecutor;
import org.da.vecu.plugin.FileLogMgr;
import org.da.vecu.plugin.ViewUtil;
import org.da.vecu.plugin.SconsLogView.PrintType;
import org.eclipse.e4.core.di.annotations.Execute;

public class SconsRunBtnCommand {
	class SconsCmdThread extends Thread
	{
		public void run()
		{
			String consoleLog = CommandExecutor.Run();
			ViewUtil.Print2(PrintType.Info, consoleLog);
			ViewUtil.Print2(PrintType.Info, "Run Successful.");
			FileLogMgr.Logging("Run", consoleLog);
		}
	}
	@Execute
	public void execute() {
		SconsCmdThread thread = new SconsCmdThread();
		thread.start();
	}
	
}