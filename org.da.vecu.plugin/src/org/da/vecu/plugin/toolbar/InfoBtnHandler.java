package org.da.vecu.plugin.toolbar;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class InfoBtnHandler {
    @Execute
    public void execute(Shell shell) {
        new InfoDialog(shell).open();
    }

    private class InfoDialog extends Dialog {

        protected InfoDialog(Shell parentShell) {
            super(parentShell);
        }

        @Override
        protected Control createDialogArea(Composite parent) {
            Composite container = (Composite) super.createDialogArea(parent);
            container.setLayoutData(new GridData(GridData.FILL_BOTH));
            container.setLayout(new GridLayout(1, false));

            Label titleLabel = new Label(container, SWT.NONE);
            titleLabel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
            titleLabel.setText("Automation Verification Tool");
            titleLabel.setFont(parent.getFont()); // You can set a larger font here if needed

            Label versionLabel = new Label(container, SWT.NONE);
            versionLabel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
            versionLabel.setText("Version: 1.0.1.1");

            Label companyLabel = new Label(container, SWT.NONE);
            companyLabel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
            companyLabel.setText("Company: DRIMAES inc.");

            return container;
        }

        @Override
        protected void createButtonsForButtonBar(Composite parent) {
            createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        }

        @Override
        protected Point getInitialSize() {
            return new Point(300, 200);
        }

        @Override
        protected void configureShell(Shell newShell) {
            super.configureShell(newShell);
            newShell.setText("About");
        }
    }
}