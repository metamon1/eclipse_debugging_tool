package org.da.vecu.plugin;

import java.util.List;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.util.Vector;

import org.da.vecu.plugin.IniData.BoardType;
import org.da.vecu.plugin.SconsLogView.PrintType;

public class AvtEnvIniParser {
	private static String currentBoard = "stm32f407zg";
	private static boolean useMonitoring = false;
	
    public static void setCurrentBoard(String boardName) {
        currentBoard = BoardType.fromString(boardName.toLowerCase()).name().toLowerCase();
        ImportIni();
    }

    public static String getCurrentBoard() {
        return BoardType.valueOf(currentBoard.toUpperCase()).getBoardName();
    }
    
    private static String getIniPath() {
        return String.format("avtenv_%s.ini", currentBoard.toLowerCase());
    }
    
	private static Vector<IniData> vIniData = new Vector<IniData>();

	public static List<String> GetCmdRun() {
		ImportIni();
		if(currentBoard.equalsIgnoreCase("stm32f407zg") && useMonitoring) {
			return FindIniData(IniData.KEY_BUILD_CMD_MONITORING_RUN).GetValueList();
		} else {
			return FindIniData(IniData.KEY_BUILD_CMD_RUN).GetValueList();
		}
	}

	public static List<String> GetCmdBuild() {
		ImportIni();
		return FindIniData(IniData.KEY_BUILD_CMD_BUILD).GetValueList();
	}

	public static List<String> GetCmdClean() {
		ImportIni();
		return FindIniData(IniData.KEY_BUILD_CMD_CLEAN).GetValueList();
	}

	public static List<String> GetCmdLinSock() {
		ImportIni();
		return FindIniData(IniData.KEY_BUILD_CMD_LINSOCK).GetValueList();
	}

	public static List<String> GetCmdStudio() {
		ImportIni();
		return FindIniData(IniData.KEY_BUILD_CMD_STUDIO).GetValueList();
	}

	public static String GetIniExportBoard() {
		ImportIni();
		return FindIniData(IniData.KEY_EXPORT_BOARD).GetValueString();
	}
	
	public static String GetIniConnMgrClientPath() {
		ImportIni();
		return FindIniData(IniData.KEY_CONNMGRCLIENT_PATH).GetValueString();
	}
	
	private static IniData FindIniData(String key) {
		for (int i = 0; i < vIniData.size(); i++) {
			IniData data = vIniData.get(i);
			if (data.Key.equals(key))
				return data;
		}
		return null;
	}

	
    private static void ImportIni() {
        InitIniData();
        Properties prop = GetIniProp();
        for (int i = 0; i < vIniData.size(); i++) {
            IniData data = vIniData.get(i);
            data.CurrentValue = prop.getProperty(data.Key, data.DefaultValue);
        }
	}

	private static void InitIniData() {
		vIniData.removeAllElements();
	    IniData.BoardType boardType = IniData.BoardType.valueOf(currentBoard.toUpperCase());
	    vIniData.add(new IniData(IniData.KEY_EXPORT_BOARD, currentBoard));
	    vIniData.add(new IniData(IniData.KEY_BUILD_CMD_BUILD, boardType.getDefaultValue(IniData.KEY_BUILD_CMD_BUILD)));
	    vIniData.add(new IniData(IniData.KEY_BUILD_CMD_CLEAN, boardType.getDefaultValue(IniData.KEY_BUILD_CMD_CLEAN)));
		vIniData.add(new IniData(IniData.KEY_BUILD_CMD_RUN, boardType.getDefaultValue(IniData.KEY_BUILD_CMD_RUN)));
		vIniData.add(new IniData(IniData.KEY_BUILD_CMD_MONITORING_RUN, boardType.getDefaultValue(IniData.KEY_BUILD_CMD_MONITORING_RUN)));
		vIniData.add(new IniData(IniData.KEY_BUILD_CMD_STUDIO, boardType.getDefaultValue(IniData.KEY_BUILD_CMD_STUDIO)));
		vIniData.add(new IniData(IniData.KEY_BUILD_CMD_LINSOCK, boardType.getDefaultValue(IniData.KEY_BUILD_CMD_LINSOCK)));
		vIniData.add(new IniData(IniData.KEY_CONNMGRCLIENT_PATH, boardType.getDefaultValue(IniData.KEY_CONNMGRCLIENT_PATH)));
	}

	private static Properties GetIniProp() {
	    Properties prop = new Properties();
	    String currentPath = System.getProperty("user.dir") + "/" + getIniPath();
	    File f = new File(getIniPath());
	    ViewUtil.Print2(PrintType.Debug, f.getPath());
	    if (!f.exists()) {
	        try {
	            CreateIniFile(f);
	            ViewUtil.Print2(PrintType.Info, String.format("[Ini]Created an ini file.[%s]", currentPath));
	        } catch (IOException e) {
	            ViewUtil.Print2(PrintType.Error, String.format("[Ini]Failed to create ini file.[%s]", currentPath));
	            return prop;
	        }
	    } else {
	        ViewUtil.Print2(PrintType.Info, String.format("[Ini]ini file was imported successfully.[%s]", currentPath));
	    }

	    try {
	        prop.load(new FileInputStream(getIniPath()));
	    } catch (IOException e) {
	        e.printStackTrace();
	        ViewUtil.Print2(PrintType.Error, String.format("[Ini]Failed to read ini file.[%s]", currentPath));
	    }
	    return prop;
	}

	private static void CreateIniFile(File f) throws IOException {
		StringBuilder sb = new StringBuilder();
		sb.append("[configuration]\n");
	    IniData.BoardType boardType = IniData.BoardType.valueOf(currentBoard.toUpperCase());
	    
	    sb.append(String.format("export_board=%s\n", currentBoard));
	    sb.append(String.format("cmd_build=%s\n", boardType.getDefaultValue(IniData.KEY_BUILD_CMD_BUILD)));
	    sb.append(String.format("cmd_clean=%s\n", boardType.getDefaultValue(IniData.KEY_BUILD_CMD_CLEAN)));
	    sb.append(String.format("cmd_run=%s\n", boardType.getDefaultValue(IniData.KEY_BUILD_CMD_RUN)));
	    sb.append(String.format("cmd_monitoring_run=%s\n", boardType.getDefaultValue(IniData.KEY_BUILD_CMD_MONITORING_RUN)));
	    sb.append(String.format("cmd_studio=%s\n", boardType.getDefaultValue(IniData.KEY_BUILD_CMD_STUDIO)));
	    sb.append(String.format("cmd_linsock=%s\n", boardType.getDefaultValue(IniData.KEY_BUILD_CMD_LINSOCK)));
	    sb.append(String.format("conn_mgr_client_path=%s\n", boardType.getDefaultValue(IniData.KEY_CONNMGRCLIENT_PATH)));

		f.createNewFile();
		BufferedWriter writer = new BufferedWriter(new FileWriter(f));
		writer.write(sb.toString());
		writer.close();
	}
	
    public static void setUseMonitoring(boolean use) {
        useMonitoring = use;
    }

    public static boolean isUseMonitoring() {
        return useMonitoring;
    }

}
