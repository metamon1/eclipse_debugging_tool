package org.da.vecu.plugin;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import org.da.vecu.plugin.SconsLogView.PrintType;

public class CommandExecutor {
	private static final String ENV_KEY_BOARD = "BOARD";
	public static String ENV_VALUE_BOARD = "stm32f407zg";
	// private static final String ENV_VALUE_BOARD = "posix";
	private static final String ENV_KEY_RELEASE = "RELEASE";
	private static final String ENV_VALUE_RELEASE = "ascore";

	enum OS_TYPE {
		Windows, Linux, None
	}

	private static OS_TYPE os_type = OS_TYPE.None;
	private static String newline = "\n";

	public CommandExecutor() {
	}

	private static void InitOsType() {
		if (os_type != OS_TYPE.None)
			return;
		String os = System.getProperty("os.name").toLowerCase();

		os_type = OS_TYPE.None;
		if (os.equals("windows 11") || os.equals("windows 10")) {
			os_type = OS_TYPE.Windows;
			newline = "\r\n";
		} else if (os.equals("linux")) {
			os_type = OS_TYPE.Linux;
		} else {
		}
	}
	
    public static void setCurrentBoard(String board) {
        ENV_VALUE_BOARD = board;
        AvtEnvIniParser.setCurrentBoard(board);
    }

	public static String Build() {
		InitOsType();
		InitChRun();
		List<String> command = AvtEnvIniParser.GetCmdBuild();
		return Run(command);
	}

	public static String Clean() {
		InitOsType();
		InitChRun();
		List<String> command = AvtEnvIniParser.GetCmdClean();
		return Run(command);
	}

	public static String Run() {
		InitOsType();
		InitChRun();
		List<String> command = AvtEnvIniParser.GetCmdRun();
		return Run(command);
	}

	public static String LinSock() {
		InitOsType();
		InitChRun();
		List<String> command = AvtEnvIniParser.GetCmdLinSock();
		return Run(command);
	}

	public static String Studio() {
		InitOsType();
		InitChRun();
		List<String> command = AvtEnvIniParser.GetCmdStudio();
		return Run(command);
	}

	private static String Run(List<String> cmd) {
		String s = null;
		ENV_VALUE_BOARD = AvtEnvIniParser.GetIniExportBoard();
		try {
			StringBuilder stringbuilder = new StringBuilder();
			ProcessBuilder pb = new ProcessBuilder(cmd);

			pb.directory(new File(Paths.get("").toAbsolutePath().toString()));
			Map<String, String> env = pb.environment();
			env.put(ENV_KEY_BOARD, ENV_VALUE_BOARD);
			env.put(ENV_KEY_RELEASE, ENV_VALUE_RELEASE);

			Process process = pb.start();
			BufferedReader stdOut = new BufferedReader(new InputStreamReader(process.getInputStream()));
			//BufferedReader stdErr = new BufferedReader(new InputStreamReader(process.getErrorStream()));

			while ((s = stdOut.readLine()) != null) {
				stringbuilder.append(s).append(newline);
			}
			stringbuilder.append(newline);
/*
			while ((s = stdErr.readLine()) != null) {
				stringbuilder.append(s).append(newline);
			}
			*/
			return stringbuilder.toString();
		} catch (IOException e) {
			e.printStackTrace();
			ViewUtil.Print2(PrintType.Error, e.toString());
		}

		return "Error executing command";
	}

	static boolean init = false;

	public static boolean InitChRun() {
		if (init)
			return true;
		init = true;
		if (Run2(List.of("modprobe", "can_raw")) == false) {
			return false;
		}
		if (Run2(List.of("modprobe", "can")) == false) {
			return false;
		}
		if (Run2(List.of("modprobe", "vcan")) == false) {
			return false;
		}
		if (Run2(List.of("ip", "link", "add", "dev", "vcan0", "type", "vcan")) == false) {
			return false;
		}
		if (Run2(List.of("ip", "link", "add", "dev", "vcan1", "type", "vcan")) == false) {
			return false;
		}
		if (Run2(List.of("ip", "link", "set", "vcan0", "up")) == false) {
			return false;
		}
		if (Run2(List.of("ip", "link", "set", "vcan1", "up")) == false) {
			return false;
		}
		if (Run2(List.of("ip", "link", "set", "vcan0", "mtu", "72")) == false) {
			return false;
		}
		return true;
	}

	private static boolean Run2(List<String> cmd) {
		ProcessBuilder pb = new ProcessBuilder(cmd);
		pb.directory(new File(Paths.get("").toAbsolutePath().toString()));

		try {
			Process process = pb.start();
			process.waitFor();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/*
	 * 
	 * public static String SconsBuild() { if (os_type == OS_TYPE.None)
	 * InitOsType(); List<String> command = List.of("scons"); return Run(command); }
	 * public static String SconsClean() { if (os_type == OS_TYPE.None)
	 * InitOsType(); List<String> command = List.of("scons", "-c"); return
	 * Run(command); }
	 * 
	 * public static String SconsRun() { if (os_type == OS_TYPE.None) InitOsType();
	 * List<String> command = List.of("scons", "run"); return Run(command); } public
	 * static String SconsStudio() { if (os_type == OS_TYPE.None) InitOsType();
	 * List<String> command = List.of("scons", "studio"); return Run(command); }
	 * 
	 * public static String RunLinChannel() { if (os_type == OS_TYPE.None)
	 * InitOsType(); List<String> command =
	 * List.of("./build/posix/socket_lin_driver.exe", "0"); return Run(command); }
	 */
}
