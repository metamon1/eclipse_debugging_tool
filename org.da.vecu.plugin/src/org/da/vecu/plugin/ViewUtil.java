package org.da.vecu.plugin;

import org.da.vecu.plugin.SconsLogView.PrintType;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

public class ViewUtil {
	public static final boolean DEBUG = true;
	/*
	public static SconsLogView getSconsLogViewInstance() {
		try {
			IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			IViewPart viewPart = activePage.findView(SconsLogView.ID);
			SconsLogView view = (SconsLogView) viewPart.getAdapter(SconsLogView.class);
			return view;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void showSconsLogView() {
		try {
			IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			activePage.showView(SconsLogView.ID);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	public static void Print(PrintType type, String s) {
		IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		IViewPart viewPart = activePage.findView(SconsLogView.ID);
		if (viewPart == null) {
			try {
				activePage.showView(SconsLogView.ID);
				viewPart = activePage.findView(SconsLogView.ID);
			} catch (PartInitException e) {
				e.printStackTrace();
				return;
			}
		}
		SconsLogView view = viewPart.getAdapter(SconsLogView.class);
		if (view == null)
			return;
		view.Print(type, s);
	}

	public static void Print2(PrintType type, String s) {
	   	 Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				Print(type,s);				
			}
	   	 });
	}
}
