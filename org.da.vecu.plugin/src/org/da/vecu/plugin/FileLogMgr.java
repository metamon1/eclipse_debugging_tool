package org.da.vecu.plugin;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import org.da.vecu.plugin.SconsLogView.PrintType;

public class FileLogMgr {

	public FileLogMgr() {

	}

	public static void Logging(String name, String log) {
		//create dir
		File logDir = new File("./Log/");
		if(logDir.exists() == false)
		{
			logDir.mkdir();
			ViewUtil.Print2(PrintType.Info, String.format("Log folder has been created. [%s]",logDir.getAbsolutePath()));
		}
		
		//create log file
		name = name.replace(' ', '_');
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		SimpleDateFormat tsFormat = new SimpleDateFormat("yyMMdd_hhmmss");
		String sTimestamp = tsFormat.format(ts);
		
		String fileName = String.format("%s_%s.log", name, sTimestamp);
		String filePath = String.format("%s/%s",logDir.getPath(),fileName);
		
		File logFile = new File(filePath);
		//ViewUtil.Print2(PrintType.Debug, logFile.getPath());
		try {
			logFile.createNewFile();
		} catch (IOException e) {
			ViewUtil.Print2(PrintType.Error, "[Logging]File creation failed.");
		}

		//write log
		try {
			FileWriter fw = new FileWriter(logFile);
			BufferedWriter writer = new BufferedWriter(fw);
			writer.write(log);
			writer.close();
		} catch (IOException e) {
			ViewUtil.Print2(PrintType.Error, "[Logging]Writing log file failed.");
		}
	}
}
