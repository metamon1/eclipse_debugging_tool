import socket
import threading
from scapy.all import IP, TCP, Raw, send, Ether

def send_tcp_packet(host, port, packet):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))
        s.sendall(bytes(packet))
        # response = s.recv(1024)
        # print(f"Received: {response.decode()}")

def create_tcp_packet(src_ip, dst_ip, src_port, dst_port, payload):
    ip = IP(src=src_ip, dst=dst_ip)
    tcp = TCP(sport=src_port, dport=dst_port)
    packet = ip/tcp/Raw(load=payload)
    return packet

def dissect_tcp_packet_from_bytes(packet_bytes):
    # 바이트 데이터를 패킷으로 파싱
    packet = IP(packet_bytes)
    
    # IP 레이어와 TCP 레이어 추출
    if IP in packet:
        ip_layer = packet[IP]
        tcp_layer = packet[TCP]
        
        src_ip = ip_layer.src
        dst_ip = ip_layer.dst
        src_port = tcp_layer.sport
        dst_port = tcp_layer.dport
        
        # Raw 데이터가 있을 경우에만 페이로드를 추출
        if Raw in packet:
            payload = packet[Raw].load.decode(errors='ignore')
        else:
            payload = ""
        
        return src_ip, dst_ip, src_port, dst_port, payload
    else:
        raise ValueError("Not a valid IP/TCP packet")


class client:
    def __init__(self) -> None:
        self.hold = False

    def start_client(self,host='127.0.0.1', port=1104):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((host, port))
            print(f"Connected to server at {host}:{port}")

            client_thread = threading.Thread(target=self.recv_msg, args=(s,))
            client_thread.start()

            is_first = True

            while True:
                try:

                    # group A를 최초 전송
                    if(is_first):
                        str_data = "C"
                        s.sendall(str_data.encode())
                        is_first = False
                        continue

                    message = input("Enter message to send (type 'exit' to close): ")
                    if message.lower() == 'exit':
                        exit()

                    # if(is_first):
                    #     s.sendall(message.encode())
                    #     is_first = False
                    #     continue

                    src_ip1 = "192.168.10.5"
                    dst_ip1 = "192.168.10.2"
                    src_port1 = 12345
                    dst_port1 = 80
                    payload1 = message
                    
                    tcp1_packet = create_tcp_packet(src_ip1, dst_ip1, src_port1, dst_port1, payload1)

                    s.sendall(bytes(tcp1_packet))
                    self.hold = True

                except KeyboardInterrupt:
                    exit()

                

    def recv_msg(self,socket):
        is_first = True
        while True:
            try:
                data = socket.recv(1024)

                if(is_first):
                    is_first = False
                    print(f"Received from server: {data.decode()}")
                    continue

                src_ip, dst_ip, src_port, dst_port, payload = dissect_tcp_packet_from_bytes(data)

                print(f"###########client################")
                print(f"Source IP: {src_ip}")
                print(f"Destination IP: {dst_ip}")
                print(f"Source Port: {src_port}")
                print(f"Destination Port: {dst_port}")
                print(f"Payload: {payload}")

                

            except KeyboardInterrupt:
                exit()

if __name__ == "__main__":
    client().start_client()


# if __name__ == "__main__":
#     # TCP1 패킷 생성
#     src_ip1 = "192.168.1.2"
#     dst_ip1 = "192.168.1.3"
#     src_port1 = 12345
#     dst_port1 = 80
#     payload1 = "This is the payload of TCP1"
    
#     tcp1_packet = create_tcp_packet(src_ip1, dst_ip1, src_port1, dst_port1, payload1)

    # 패킷 분해
    # src_ip, dst_ip, src_port, dst_port, payload = dissect_tcp_packet_from_bytes(bytes(tcp1_packet))

    # print(f"Source IP: {src_ip}")
    # print(f"Destination IP: {dst_ip}")
    # print(f"Source Port: {src_port}")
    # print(f"Destination Port: {dst_port}")
    # print(f"Payload: {payload}")

    # 서버로 페이로드 전송
    # send_tcp_packet('127.0.0.1', 12345, tcp1_packet)


