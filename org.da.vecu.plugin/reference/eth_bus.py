import socket
import threading
import queue
import csv

from scapy.all import IP, TCP, Raw

class TCPConnectManager:
    def __init__(self, host='127.0.0.1', port=1104):
        self.host = host
        self.port = port
        self.clients = []
        self.is_running = False
        self.observers = []

    def start_server(self):
        self.is_running = True
        server_thread = threading.Thread(target=self.run_server)
        server_thread.start()
        return server_thread

    def run_server(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((self.host, self.port))
            s.listen()
            print(f"Server started, listening on {self.host}:{self.port}")
            while self.is_running:
                try:
                    conn, addr = s.accept()
                    self.clients.append((conn, addr))
                    self.notify_observers(conn, addr,self.clients)
                except KeyboardInterrupt:
                    print("Server is shutting down.")
                    self.is_running = False
                    break
                except Exception as e:
                    print(f"Error accepting client connection: {e}")

    def notify_observers(self, conn, addr, clients):
        for observer in self.observers:
            observer.update(conn, addr, clients)

    def add_observer(self, observer):
        self.observers.append(observer)

    def remove_observer(self, observer):
        self.observers.remove(observer)

class TCPDataManager:
    def __init__(self):
        self.data_queue = queue.Queue()
        self.is_running = True
        self.ip_table = {}

        with open('ip_table.csv', 'r', encoding='utf-8-sig') as f:
            reader = csv.reader(f)
            for line in reader:
                self.ip_table[line[1]] = line[0] # line[0]: index / line[1]: ip

        print(f"ip table: {self.ip_table}")

    def dissect_tcp_packet_from_bytes(self,packet_bytes):
        # 諛붿씠�� �곗씠�곕� �⑦궥�쇰줈 �뚯떛
        packet = IP(packet_bytes)
        
        # IP �덉씠�댁� TCP �덉씠�� 異붿텧
        if IP in packet:
            ip_layer = packet[IP]
            tcp_layer = packet[TCP]
            
            src_ip = ip_layer.src
            dst_ip = ip_layer.dst
            src_port = tcp_layer.sport
            dst_port = tcp_layer.dport
            
            # Raw �곗씠�곌� �덉쓣 寃쎌슦�먮쭔 �섏씠濡쒕뱶瑜� 異붿텧
            if Raw in packet:
                payload = packet[Raw].load.decode(errors='ignore')
            else:
                payload = ""
            
            return src_ip, dst_ip, src_port, dst_port, payload
        else:
            raise ValueError("Not a valid IP/TCP packet")
    
class TCPClientManager:
    def __init__(self, data_manager:TCPDataManager):
        self.data_manager = data_manager
        self.clients_index = {}

    def update(self, conn, addr, clients):
        client_thread = threading.Thread(target=self.handle_client, args=(conn, addr, clients))
        client_thread.start()
    
    def handle_client(self, conn, addr, clients):
        print(f"Connected by {addr}")
        is_first = True
        client_index = ""

        with conn:
            while True:
                data = conn.recv(1024)
                if not data:
                    print("close client")
                    clients.remove((conn,addr))
                    if(len(self.clients_index)):
                        del self.clients_index[conn]
                    break

                # 泥� �곗씠�곕뒗 援щ텇�� �� �� �덈뒗 �몄옄瑜� �섍꺼 以���.
                if(is_first):
                    print(f"Received from {addr}: {data.decode()}")
                    for key, value in self.data_manager.ip_table.items():
                        if(value == data.decode()):
                            client_index = data.decode()
                            self.clients_index[conn] = client_index
                            break

                    if(client_index == ""):
                        print("not match index!")
                        clients.remove((conn,addr))
                        if(len(self.clients_index)):
                            del self.clients_index[conn]
                        break

                    print(f"client_index: {client_index}")
                    conn.sendall(str("OK").encode())
                    is_first = False
                    continue
                

                # �⑦궥 遺꾪빐
                src_ip, dst_ip, src_port, dst_port, payload = self.data_manager.dissect_tcp_packet_from_bytes(data)

                print(f"###########BUS################")
                print(f"Source IP: {src_ip}")
                print(f"Destination IP: {dst_ip}")
                print(f"Source Port: {src_port}")
                print(f"Destination Port: {dst_port}")
                print(f"Payload: {payload}")
                
                # print(f"Received from {addr}: {data.decode()}")
                # self.data_manager.add_data((addr, data))

                # ip table�� dst_ip媛� 議댁옱 �� 寃쎌슦 泥� csv�뚯씪�먯꽌 ip�� 鍮꾧탳 �섏뿬
				# �대떦�섎뒗 group�쇰줈 �곗씠�� �꾩넚
                if dst_ip in self.data_manager.ip_table:
                    group_index = self.data_manager.ip_table[dst_ip]
                    print(f'Key: {dst_ip}, group_index: {group_index}') # group_index: A B
                else:
                    print(f'Key: {dst_ip} not found')
                    continue

                for client in clients:
                    if client[0] != conn:
                        if self.clients_index[client[0]] == group_index:
                            client[0].sendall(data)

if __name__ == "__main__":
    data_manager = TCPDataManager()
    client_manager = TCPClientManager(data_manager)
    connect_manager = TCPConnectManager()

    connect_manager.add_observer(client_manager)

    server_thread = connect_manager.start_server()
    # processor_thread = data_manager.start_data_processor()

    try:
        server_thread.join()
    except KeyboardInterrupt:
        connect_manager.is_running = False
        data_manager.is_running = False
        print("Shutting down server...")
